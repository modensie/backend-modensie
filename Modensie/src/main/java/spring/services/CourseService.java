package spring.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import spring.entities.Course;
import spring.repositories.CourseRepository;

import java.util.List;

@Service
public class CourseService {

    @Autowired
    private CourseRepository courseRepository;

    // ADD
    public void addCourse(Course course){
        Course newCourse = new Course(course.getName(), course.getPassword());
        courseRepository.save(newCourse);
    }

    // FIND
    public List<Course> findAllCourses() {
        List<Course> courses = courseRepository.findAll();
        return courses;
    }

    public List<String> getAllCoursesName() {
        List<String> courses = courseRepository.getAllCourses();
        return courses;
    }

    public Course checkCourse(String courseName) {
        Course course = courseRepository.findByCourseName(courseName);
        return course;
    }

    // DELETE
    public void deleteCourse(String courseName)
    {
        Course course = courseRepository.findByCourseName(courseName);
        courseRepository.delete(course);
    }

    // EDIT
    public void editCoursePassword(Course coursePassword) {
        Course course = courseRepository.findByCourseName(coursePassword.getName());
        course.setPassword(coursePassword.getPassword());
        courseRepository.save(course);
    }
}
