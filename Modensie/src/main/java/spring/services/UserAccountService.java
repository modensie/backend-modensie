package spring.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import spring.dto.UserEmailAddressDTO;
import spring.entities.UserAccount;
import spring.dto.UserLoginDTO;
import spring.repositories.UserAccountRepository;

import java.util.List;
import java.util.UUID;

@Service
public class UserAccountService {

    @Autowired
    private UserAccountRepository userRepository;

    // ADD
    public void registerUser(UserAccount userAccount){
        UserAccount user= new UserAccount(userAccount.getUsername(),userAccount.getPassword(),userAccount.getName(),userAccount.getBirthdate(),userAccount.getGender(),userAccount.getEmailAddress(),userAccount.getRole());
        userRepository.save(user);
    }

    // DELETE
    public void deleteUser(String username)
    {
        UserAccount user = userRepository.findByUsername(username);
        userRepository.delete(user);
    }

    // FIND
    public UUID findAccountId(String userUsername) {
        return userRepository.findByUsername(userUsername).getUserAccountId();
    }

    public UserAccount findAccountDetailsFromUsername(String userUsername) {
        return userRepository.findByUsername(userUsername);
    }

    public List<UserAccount> findAllUserAccount() {
        List<UserAccount> users = userRepository.findAll();
        return users;
    }

    public List<String> findAllTeachers() {
        return userRepository.findAllTeachers();
    }
    public List<String> findAllStudents() {
        return userRepository.findAllStudents();
    }

    // EDIT (DTO used)
    public void editUserEmailAddress(UserEmailAddressDTO userEmailAddress) {
        UserAccount user = userRepository.findByUsername(userEmailAddress.getUsername());
        user.setEmailAddress(userEmailAddress.getEmailAddress());
        userRepository.save(user);
    }

    public void editUserPassword(UserLoginDTO userLogin) {
        UserAccount user = userRepository.findByUsername(userLogin.getUsername());
        user.setPassword(userLogin.getPassword());
        userRepository.save(user);
    }
}