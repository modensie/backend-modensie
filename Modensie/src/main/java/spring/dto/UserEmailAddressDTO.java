package spring.dto;

public class UserEmailAddressDTO
{
    private String username;
    private String emailAddress;

    // Constructors
    public UserEmailAddressDTO() { }

    public UserEmailAddressDTO(String username, String emailAddress) {
        this.username = username;
        this.emailAddress = emailAddress;
    }

    // Getters and Setters
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }
}
