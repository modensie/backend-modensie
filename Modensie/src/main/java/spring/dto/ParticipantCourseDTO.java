package spring.dto;


public class ParticipantCourseDTO
{
    private String courseName;
    private String usernameUser;

    // Constructors
    public ParticipantCourseDTO() {
    }

    public ParticipantCourseDTO(String courseName, String usernameUser) {
        this.courseName = courseName;
        this.usernameUser = usernameUser;
    }

    // Getters and Setters
    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public String getUsernameUser() {
        return usernameUser;
    }

    public void setUsernameUser(String usernameUser) {
        this.usernameUser = usernameUser;
    }
}
