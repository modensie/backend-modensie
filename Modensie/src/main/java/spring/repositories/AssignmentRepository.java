package spring.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import spring.entities.Assignment;

import java.util.List;

@Repository
public interface AssignmentRepository extends JpaRepository<Assignment,Integer> {

    @Query(value="SELECT assignment.name, user_account.name AS teacher, date, type, description FROM assignment " +
            "JOIN course ON course.course_id = assignment.course_id " +
            "JOIN user_account ON user_account_id = assignment.user_id " +
            "WHERE course.name = (?1)", nativeQuery = true)
    public List<Object[]> findAllAssigmentOfCourse(String courseName);

    @Query(value="SELECT assignment.* FROM assignment " +
            "JOIN course ON course.course_id = assignment.course_id " +
            "WHERE assignment.name = (?1) AND course.name = (?2)", nativeQuery = true)
    public Assignment checkAssignmentAssignedToCourse(String assignmentName, String courseName);

    @Query(value="SELECT assignment.* FROM assignment JOIN course ON course.course_id = assignment.course_id " +
            "WHERE assignment.name = (?1) AND course.name = (?2)", nativeQuery = true)
    public Assignment findByAssignmentName(String assignmentName, String courseName);
}
