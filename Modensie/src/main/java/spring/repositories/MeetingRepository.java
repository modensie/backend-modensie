package spring.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import spring.entities.Meeting;

import java.util.List;

@Repository
public interface MeetingRepository extends JpaRepository<Meeting,Integer> {

    @Query(value="SELECT course.name AS courseName, meeting.* FROM meeting JOIN course ON meeting.course_id = course.course_id " +
            "JOIN participant_course ON course.course_id = participant_course.course_id " +
            "JOIN user_account ON user_account.user_account_id = participant_course.user_id " +
            "    WHERE user_account.username = (?1)", nativeQuery = true)
    public List<Object[]> findAllMeetingsOfUser(String username);
}
