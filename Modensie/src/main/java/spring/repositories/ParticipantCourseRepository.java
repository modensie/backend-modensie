package spring.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import spring.entities.ParticipantCourse;

import java.util.List;

@Repository
public interface ParticipantCourseRepository extends JpaRepository<ParticipantCourse,Integer> {

    @Query(value="SELECT course.name FROM course JOIN participant_course ON course.course_id = participant_course.course_id " +
            "JOIN user_account ON user_account.user_account_id = participant_course.user_id " +
            "WHERE user_account.username = (?1)", nativeQuery = true)
    public List<String> findAllCoursesOfUser(String username);

    @Query(value="SELECT course.name FROM course WHERE course_id NOT IN ( " +
            "SELECT course.course_id FROM course JOIN participant_course ON course.course_id = participant_course.course_id " +
            "JOIN user_account ON user_account.user_account_id = participant_course.user_id " +
            "WHERE user_account.username = (?1))", nativeQuery = true)
    public List<String> findAllCoursesNotFollowedByUser(String username);

    @Query(value="SELECT user_account.* FROM user_account JOIN participant_course ON user_account_id = user_id " +
            "JOIN course ON course.course_id = participant_course.course_id " +
            "WHERE course.name = (?1)", nativeQuery = true)
    public List<Object[]> findAllParticipantsOfCourse(String courseName);

    @Query(value="SELECT username FROM user_account JOIN participant_course ON user_account_id = user_id " +
            "JOIN course ON course.course_id = participant_course.course_id " +
            "WHERE user_account.role = 'student' AND course.name = (?1)", nativeQuery = true)
    public List<String> findAllStudentsOfCourse(String courseName);

    @Query(value="SELECT username FROM user_account JOIN participant_course ON user_account_id = user_id " +
            "JOIN course ON course.course_id = participant_course.course_id " +
            "WHERE user_account.role ='teacher' AND course.name = (?1)", nativeQuery = true)
    public List<String> findAllTeachersOfCourse(String courseName);

    @Query(value="SELECT participant_course.* FROM course JOIN participant_course ON course.course_id = participant_course.course_id " +
            "JOIN user_account ON user_account.user_account_id = participant_course.user_id " +
            "WHERE user_account.username = (?1) AND course.name = (?2)", nativeQuery = true)
    public ParticipantCourse checkUserAssignedToCourse(String username, String courseName);
}
