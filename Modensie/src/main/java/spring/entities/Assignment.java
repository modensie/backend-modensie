package spring.entities;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@Entity
@Table(name="assignment")
public class Assignment {

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Column(name = "assignment_id", columnDefinition = "BINARY(16)")
    private UUID assignmentId;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "course_id", nullable = false)
    @JsonIgnore
    private Course courseId;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "user_id", nullable = false)
    @JsonIgnore
    private UserAccount userId;

    @Column(name = "name")
    private String name;

    @JsonFormat(pattern="dd-MM-yyyy")
    @Column(name = "date")
    private Date date;

    @Column(name = "type")
    private Integer type;

    @Column(name = "description", nullable = true)
    private String description;

    // Constructors
    public Assignment() {
    }

    public Assignment(UUID assignmentId, Course courseId, UserAccount userId, String name, Date date, Integer type, String description) {
        this.assignmentId = assignmentId;
        this.courseId = courseId;
        this.userId = userId;
        this.name = name;
        this.date = date;
        this.type = type;
        this.description = description;
    }

    public Assignment(Course courseId, UserAccount userId, String name, Date date, Integer type, String description) {
        this.courseId = courseId;
        this.userId = userId;
        this.name = name;
        this.date = date;
        this.type = type;
        this.description = description;
    }

    // Getters and Setters
    public UUID getAssignmentId() {
        return assignmentId;
    }

    public void setAssignmentId(UUID assignmentId) {
        this.assignmentId = assignmentId;
    }

    public Course getCourseId() {
        return courseId;
    }

    public void setCourseId(Course courseId) {
        this.courseId = courseId;
    }

    public UserAccount getUserId() {
        return userId;
    }

    public void setUserId(UserAccount userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}