# Backend Modensie

Backend Application for Modensie Platform (Spring Boot)

**Server on localhost:8080**

## Setup
1. Install Maven using `sudo apt-get install maven`.
2. Now, execute the `mvn spring-boot:run` command in the backend application project root folder _(Backend Moedensie\Modensie)_ which triggers the download of Apache Tomcat and initializes the startup of Tomcat.

## MariaDB or MySQL server
1. Make changes in the _Backend Modensie\Modensie\pom.xml_ file. Look for **MySQL Dependency** or **MariaDB Dependency** and choose the one you are using _(comment/uncomment the lines)_.
2. Make changes in the _Backend Modensie\Modensie\src\main\resources\application.properties_ file. Look for **DataSource Configuration (MySQL)** or **DataSource Configuration (MariaDB)** and choose the one you are using _(comment/uncomment the lines)_.

## CORS policy (Optional)
If you want to restrict data manipulation through backend application, change this line `@CrossOrigin(origins = "*")` from all _Backend Modensie\Modensie\src\main\java\spring\controller\[name]Controller_ files with your frontend server, _e.g. @CrossOrigin(origins = "http://192.168.1.23:4200")_. All the requests from your tomcat server to the spring boot server will be granted. The default state of Cross Origin is `@CrossOrigin(origins = "*")` which allows all connections.

## HTTPS*
1. To make the server run on HTTPS port, uncomment the _#HTTPS#_ section from the _Backend Modensie\Modensie\src\main\resources\application.properties_ file.
1.1. Make sure the Fronted App send https requests to Backend App, or run the angular client server on https port `ng serve --ssl`.
2. In order to redirect HTTP requests to HTTPS, uncomment the code from the _Backend Modensie\Modensie\src\main\java\spring\ServerConfig.java_ file, so the Tomcat server will open the 8443 (http) port and 8080 (https) port.

_*Note: Certificate for your browser is located in this file: Backend Modensie\Modensie\src\main\resources\myCertificate.crt_

## Errors
In case you have the following error when you're trying to run the backend:
```
java.sql.SQLException: Could not connect to address=(host=localhost)(port=3306)(type=master) : RSA public key is not available client side (option serverRsaPublicKeyFile not set)
```
In order to allow the client to automatically request the public key from the server, please change the following line inside the _Backend Modensie\Modensie\src\main\resources\application.properties_ file.

Change this line: `spring.datasource.url=jdbc:mariadb://localhost:3306/ModensiePlatformDB`

To this: `spring.datasource.url=jdbc:mariadb://localhost:3306/ModensiePlatformDB?allowPublicKeyRetrieval=true&useSSL=false`
